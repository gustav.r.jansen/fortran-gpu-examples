module fortran_cublas_interface
    use, intrinsic :: iso_c_binding, only : &
            c_int, &
            c_ptr, &
            c_double

    implicit none
    private

    public :: cublas_create
    public :: cublas_destroy
    public :: cublas_dgemm

    public :: CUBLAS_OP_N
    public :: CUBLAS_OP_T
    public :: CUBLAS_STATUS_SUCCESS

    integer(c_int), parameter :: CUBLAS_OP_N = 0
    integer(c_int), parameter :: CUBLAS_OP_T = 1
    integer(c_int), parameter :: CUBLAS_STATUS_SUCCESS = 0

    interface
        integer(c_int) function cublas_create(handle) bind(c, name="cublasCreate_v2")
            import :: c_int
            import :: c_ptr

            type(c_ptr) :: handle
        end function cublas_create

        integer(c_int) function cublas_destroy(handle) bind(c, name="cublasDestroy_v2")
            import :: c_int
            import :: c_ptr

            type(c_ptr), value :: handle
        end function cublas_destroy

        integer(c_int) function cublas_dgemm(handle, transa, transb, m, n, k, &
                    alpha, a, lda, b, ldb, beta, c, ldc) bind(c, name="cublasDgemm_v2")
            import :: c_int
            import :: c_ptr
            import :: c_double

            type(c_ptr), value :: handle
            integer(c_int), value :: transa, transb
            integer(c_int), value:: m, n, k, lda, ldb, ldc
            type(c_ptr), value :: a, b, c
            real(c_double) :: alpha, beta
        end function cublas_dgemm
    end interface
end module fortran_cublas_interface

