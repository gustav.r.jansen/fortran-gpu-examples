module fortran_cuda_interface
    use, intrinsic :: iso_c_binding, only : &
            c_int, &
            c_size_t, &
            c_ptr

    implicit none
    private

    public :: cuda_malloc
    public :: cuda_free
    public :: cuda_memcpy
    public :: cuda_memset

    public :: CUDA_SUCCESS
    public :: cudaMemcpyHostToHost
    public :: cudaMemcpyHostToDevice
    public :: cudaMemcpyDeviceToHost
    public :: cudaMemcpyDeviceToDevice
    public :: cudaMemcpyDefault

    integer, parameter :: CUDA_SUCCESS = 0
    integer, parameter :: cudaMemcpyHostToHost = 0
    integer, parameter :: cudaMemcpyHostToDevice = 1
    integer, parameter :: cudaMemcpyDeviceToHost = 2
    integer, parameter :: cudaMemcpyDeviceToDevice = 3
    integer, parameter :: cudaMemcpyDefault = 4

    interface
        integer(c_int) function cuda_malloc(dev_ptr, size_) &
                bind(c, name="cudaMalloc")
            import :: c_int
            import :: c_ptr
            import :: c_size_t

            type(c_ptr) :: dev_ptr
            integer(c_size_t), value :: size_
        end function cuda_malloc

        integer(c_int) function cuda_free(dev_ptr) &
                bind(c, name="cudaFree")
            import :: c_int
            import :: c_ptr

            type(c_ptr), value :: dev_ptr
        end function cuda_free

        integer(c_int) function cuda_memcpy(dst, src, size_, kind) &
                bind(c, name="cudaMemcpy")
            import :: c_int
            import :: c_ptr
            import :: c_size_t

            type(c_ptr), value :: dst, src
            integer(c_size_t), value :: size_
            integer(c_int), value :: kind
        end function cuda_memcpy

        integer(c_int) function cuda_memset(dev_ptr, value_, count_) bind(c, name="cudaMemset")
            import :: c_int
            import :: c_ptr
            import :: c_size_t

            type(c_ptr), value :: dev_ptr
            integer(c_int), value :: value_
            integer(c_size_t), value :: count_
        end function cuda_memset
    end interface
end module fortran_cuda_interface
