module utility
    implicit none
    private

    public :: get_result
contains
    character(len=6) function get_result(res)
        logical, intent(in) :: res

        get_result = "FAILED"
        if ( res ) get_result = "PASSED"
    end function get_result
end module utility
