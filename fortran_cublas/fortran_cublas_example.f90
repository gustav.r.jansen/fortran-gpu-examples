program example
    use, intrinsic :: iso_fortran_env, only : real64
    use, intrinsic :: iso_c_binding, only : &
            c_ptr, &
            c_sizeof, &
            c_int, &
            c_double, &
            c_loc

    use :: utility, only : get_result

    use :: fortran_cuda_interface, only : &
            cuda_malloc, &
            cuda_free, &
            cuda_memcpy, &
            cuda_memset, &
            CUDA_SUCCESS, &
            cudaMemcpyHostToDevice, &
            cudaMemcpyDeviceToHost

    use :: fortran_cublas_interface, only : &
            cublas_create, &
            cublas_destroy, &
            cublas_dgemm, &
            CUBLAS_STATUS_SUCCESS, &
            CUBLAS_OP_N

    real(real64), dimension(:,:), allocatable, target :: c, a, b
    type(c_ptr) :: cublas_handle
    type(c_ptr) :: a_dev, b_dev, c_dev
    integer(c_int) :: error

    allocate(a(10,10), b(10,10),c(10,10))
    call random_number(a)
    call random_number(b)
    c = 0.0_real64

    ! Allocate device pointers
    error = cuda_malloc(a_dev, size(a)*c_sizeof(1.0_c_double))
    write(*,*) "cudaMalloc A: "//get_result(error == CUDA_SUCCESS)
    error = cuda_malloc(b_dev, size(b)*c_sizeof(1.0_c_double))
    write(*,*) "cudaMalloc B: "//get_result(error == CUDA_SUCCESS)
    error = cuda_malloc(c_dev, size(c)*c_sizeof(1.0_c_double))
    write(*,*) "cudaMalloc C: "//get_result(error == CUDA_SUCCESS)

    ! Initialize C
    error = cuda_memset(c_dev, 0, size(c)*c_sizeof(1.0_c_double))
    write(*,*) "cudaMemset C: "//get_result(error == CUDA_SUCCESS)

    ! Copy host A and B to device
    error = cuda_memcpy(a_dev, c_loc(a), size(a)*c_sizeof(1.0_c_double), cudaMemcpyHostToDevice)
    write(*,*) "cudaMemcpy A: "//get_result(error == CUDA_SUCCESS)
    error = cuda_memcpy(b_dev, c_loc(b), size(b)*c_sizeof(1.0_c_double), cudaMemcpyHostToDevice)
    write(*,*) "cudaMemcpy B: "//get_result(error == CUDA_SUCCESS)

    ! Initialize cublas handle
    error = cublas_create(cublas_handle)
    write(*,*) "cublasCreate: "//get_result(error == CUBLAS_STATUS_SUCCESS)

    ! Call cublas
    error = cublas_dgemm(cublas_handle, CUBLAS_OP_N, CUBLAS_OP_N, 10, 10, 10, &
            1.0_c_double, a_dev, 10, b_dev, 10, 0.0_c_double, c_dev, 10)
    write(*,*) "cublasDgemm: "//get_result(error == CUBLAS_STATUS_SUCCESS)

    ! Destroy cublas handle
    error = cublas_destroy(cublas_handle)
    write(*,*) "cublasDestroy: "//get_result(error == CUBLAS_STATUS_SUCCESS)

    ! Copy device C to host
    error = cuda_memcpy(c_loc(c), c_dev, size(c)*c_sizeof(1.0_c_double), cudaMemcpyDeviceToHost)
    write(*,*) "cudaMemcpy C: "//get_result(error == CUDA_SUCCESS)

    ! Compare result of cublas_dgemm to matmul
    write(*,*) "C=A*B: "//get_result(all(abs(c-matmul(a,b)) < 1d-12))

    error = cuda_free(c_dev)
    write(*,*) "cudaFree C: "//get_result(error == CUDA_SUCCESS)
    error = cuda_free(b_dev)
    write(*,*) "cudaFree B: "//get_result(error == CUDA_SUCCESS)
    error = cuda_free(a_dev)
    write(*,*) "cudaFree A: "//get_result(error == CUDA_SUCCESS)

    deallocate(c, b, a)
end program example
